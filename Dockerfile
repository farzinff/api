FROM node:onbuild
ENV STEEMJS_URL ws://173.249.19.222:8090
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]
