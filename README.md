# Instachain REST API

## Usage: 

You can use Instachain blockchain API methods this way:

http://api.instachain.io:3000/getAccounts?names[]=amin  
http://api.instachain.io:3000/getState?path=/trending  
http://api.instachain.io:3000/getDiscussionsByHot?query={"tag":"photography","limit":"10"}  


## Steem API UI 

Test Steem REST API on Steem created by @good-karma here: 

https://esteemapp.github.io/steemapi/


# Extra functionalities
The following functionalities use APIs that are not documented in https://esteemapp.github.io/steemapi/ but are specific to Instachain.

## Auth
To authenticate a user POST to http://api.instachain.io:3000/auth/login WITH EXAMPLE PAYLOAD

```json
{"name": "<username>", "password": "<password>"}
```

If the username and password are correct, this call will return the following format
```json
{
"result": true,
"key_type": "<key_type>",
"priv_wif": "<priv_wif>"
"priv_memo_wif": "<priv_memo_wif>"
}
```

Fields explanation:
- key_type: The type of the entered password. Can be "owner", "active" or "posting"
- priv_wif: The converted wif. **This is the wif that will be used in all of the below calls.**
- priv_memo_wif: The memo wif. This wif is used only for Private Messaging.

---------------------------------------------------------

## Broadcast

**New post:**

To create new post POST to http://api.instachain.io:3000/broadcast/comment WITH EXAMPLE PAYLOAD
```json
{"parent_author": "",
 "parent_permlink": "developer",
 "author": "dnnatestins2",
 "permlink": "testins2ab",
 "title": "a test",
 "body": "bloum",
 "json_metadata": {
    "tags": [
        "steemdev",
        "developer",
        "javascript",
        "steemjs",
        "steem-dev"
    ],
    "users": [
        "amin"
    ],
    "links": [
        "\/@jfollas\/write-a-steemit-web-app-part-9-retrieving-content-with-getdiscussionsby",
        "https:\/\/esteem.ws\/"
    ],
    "image": [
        "https:\/\/steemitimages.com\/DQmZBbDGvKqM8V46NHaXoT7nZvrXhHLtTysrhixRu8eS87g\/javascriptlogo.png"
    ],
    "format": "html",
    "app": "instachain_mobile\/0.1"
},
 "wif":"5JhM8JdL1jwktNCgysGzLCEMAfULru13uKqjiB2SHx36hmD8sqd"}
```

Fields explanation:
- parent_author: Must be empty for new post
- parent_permlink: Main tag (category). This MUST be one of the tags declared for the post
- author: My **username**
- permlink: url slug. in this exmaple post url will be http://instachain.io/api-main-tag/@dnnatestins2/testins2a
- title: Post title
- body: Post body (in html or markdown)
- json_metadata: Must be an object containing the below parameters
- tags: Array of tags for the post. parent_permlink must belong in this array also
- users: Users tagged in this post
- links: [Optional] Array of links found in post body. Can be relative (e.g. \/@dnnatestins2\/transfers) or absolute
- image: URL of image that will appear in the front page for the post
- format: Can be markdown or html. Suggest html
- app: Name and version of the mobile app
- wif: The **priv_key** used for login

---------------------------------------------------------

**New reply:**

New reply is similar to new post with different parameters.
POST to http://api.instachain.io:3000/broadcast/comment WITH EXAMPLE PAYLOAD
```json
{"parent_author": "dnnatestins2",
 "parent_permlink": "testins2a",
 "author": "dnnatestins2",
 "permlink": "re-dnnatestins2-testins2a-20171217t132224115z",
 "title": "a reply",
 "body": "<strong>bold</strong> reply blouum",
 "json_metadata": {
    "tags": [
        "developer"
    ],
    "users": [
        "amin"
    ],
    "links": [],
    "format": "html",
    "app": "instachain_mobile\/0.1"
},
 "wif":"5JhM8JdL1jwktNCgysGzLCEMAfULru13uKqjiB2SHx36hmD8sqd"}
```

Fields explanation:
- parent_author: The username of the author of the post we are replying to
- parent_permlink: The permlink attribute of the post we are replying to.
- author: My **username**
- permlink: Permlink here must follow a specific format. It is be generated using a convention defined by steem. This is a javascript code that creates a permlink:
    ```javascript
    function commentPermlink(parentAuthor, parentPermlink) {
      const timeStr = new Date()
        .toISOString()
        .replace(/[^a-zA-Z0-9]+/g, "")
        .toLowerCase();
      parentPermlink = parentPermlink.replace(/(-\d{8}t\d{9}z)/g, "");
      return "re-" + parentAuthor + "-" + parentPermlink + "-" + timeStr;
    }
    ```
- title: Comment title
- body: Comment body (in html or markdown)
- json_metadata: Same as new post
- wif: The **priv_key** used for login

---------------------------------------------------------

**Voting:**

To vote POST to http://api.instachain.io:3000/broadcast/vote WITH PAYLOAD
```json
{"voter": "dnnatestins2",
 "author": "amin",
 "permlink": "vivek",
 "weight": 10000,
 "wif": "5JhM8JdL1jwktNCgysGzLCEMAfULru13uKqjiB2SHx36hmD8sqd"}
```

Fields explanation:
- voter: My **username**
- author: The username of the creator of the post
- permlink: The permlink attribute of the post
- weight: Leave at 10000
- wif: The **priv_key** used for login

---------------------------------------------------------

** Following:**

To follow a user POST to http://api.instachain.io:3000/broadcast/follow WITH PAYLOAD
```json
{ "follower": "dnnatestins2",
  "following": "vivek",
  "what": ["blog"],
  "wif": "5JhM8JdL1jwktNCgysGzLCEMAfULru13uKqjiB2SHx36hmD8sqd"
}
```

Fields explanation:
- follower: My **username**
- following: The username of the followed person
- what: Always "blog" to **follow**. Empty to **unfollow**.
- wif: The **priv_key** used for login

To unfollow a user POST to http://api.instachain.io:3000/broadcast/follow WITH PAYLOAD
```json
{ "follower": "dnnatestins2",
  "following": "vivek",
  "what": [],
  "wif": "5JhM8JdL1jwktNCgysGzLCEMAfULru13uKqjiB2SHx36hmD8sqd"
}
```

Fields explanation:
Same as follow except the "what" array is empty.

---------------------------------------------------------

** Profile update: **

To update the user profile POST to http://api.instachain.io:3000/broadcast/account_update WITH PAYLOAD
```json
{"account": "dnnatestins2",
"owner": null,
"active": null,
"posting": null,
"memo_key": "INS8WpLmcM8fHwh6VCV7cSKbom3oM6SCdE1GTRCveL3Kgn3pYTfHf",
"json_metadata": {
  "profile": {
	"profile_image": "https://www.cspire.com/resources/images/icons/icn_internet.svg",
	"cover_image": "https://cdn.arstechnica.net/wp-content/uploads/2017/03/fiber-getty.jpg",
	"name": "dnna ins 2",
	"about": "test abc 2",
	"location": "Somewhere 2",
	"website": "http://mywebsite2.com"
  }
},
"wif": "5JhM8JdL1jwktNCgysGzLCEMAfULru13uKqjiB2SHx36hmD8sqd"}
```

Fields explanation:
- account: My **username**
- owner: Only used for password change. Use **null**
- active: Only used for password change. Use **null**
- posting: Only used for password change. Use **null**
- memo_key: My **memo_key** retrieved from account profile (e.g. from http://api.instachain.io:3000/get_accounts?names[]=dnnatestins2)
- json_metadata: Must be an object containing the below parameters
- profile: Must be an object containing the below parameters. **ALL FIELDS BELOW ARE OPTIONAL. IF THEY DONT EXIST SEND ''.**
- profile_image: URL of profile picture
- cover_image: URL of cover image
- name: User display name
- about: User bio
- location: User location
- website: User website
- wif: The **priv_key** used for login

---------------------------------------------------------

** Private messaging: **

To send a private message to a user POST to http://api.instachain.io:3000/broadcast/private_message WITH PAYLOAD
```json
{"from": "mobile",
 "to": "dnna",
 "amount": "0.001 INSTA",
 "memo": "some memo",
 "priv_memo_wif": "5K18fihMUh8Q8gj4mVNbRw25akH28ztgX8EndPzUEEpcr6kSs2n",
 "wif": "5Jn8CJmWtyYMBw7PWJVg7pUcLguEnJwNPyY5ce18NuhVrHGP2SM"}
```

Fields explanation:
- from: My **username**
- to: Recipient **username**
- amount: Use "0.001 INSTA" always.
- memo: The text message
- priv_memo_wif: The **priv_memo_wif** received during login. ** This is different from priv_wif **
- wif: The **priv_wif** used for login

To see the chat history of received private messages POST to http://api.instachain.io:3000/auth/chat_history WITH PAYLOAD
```json
{
  "name": "dnna",
  "priv_memo_wif": "5K18fihMUh8Q8gj4mVNbRw25akH28ztgX8EndPzUEEpcr6kSs2n",
  "to_name": "mobile"
}
```

Fields explanation:
- name: My **username**
- priv_memo_wif: The **priv_memo_wif** received during login. ** This is different from priv_wif **

To see all conversations (users you have chatted with and the last message in each conversation) POST to http://api.instachain.io:3000/auth/chat_conversations WITH PAYLOAD
```json
{
  "name": "dnna",
  "priv_memo_wif": "5K18fihMUh8Q8gj4mVNbRw25akH28ztgX8EndPzUEEpcr6kSs2n"
}
```

Fields explanation:
- name: My **username**
- priv_memo_wif: The **priv_memo_wif** received during login. ** This is different from priv_wif **