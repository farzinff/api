const express = require('express');
const camelize = require('camelize')
const decamelize = require('decamelize');
const _ = require('lodash');
const steem = require('golos-js');
const methods = require('../node_modules/golos-js/lib/api/methods.js');
const broadcastOperations = require('../node_modules/golos-js/lib/broadcast/operations.js');
const authMethods = [ // TODO move these to ../node_modules/golos-js/lib/auth/auth_methods.js
  {
    'auth_method': 'to_wif',
    'params': [
       'name',
       'password',
       'role'
    ]
  },
  {
    'auth_method': 'verify',
    'params': [
       'name',
       'password',
       'auths'
    ]
  },
  {
    'auth_method': 'generate_keys',
    'params': [
       'name',
       'password',
       'roles'
    ]
  },
  {
    'auth_method': 'wif_is_valid',
    'params': [
       'privWif',
       'pubWif'
    ]
  }
];
const router = express.Router();

router.get('/', (req, res) => {
  res.redirect('https://esteemapp.github.io/steemapi/');
});

router.post('/rpc', (req, res) => {
  const { method, params, id } = req.body;
  const mapping = _.filter(methods, { method: method });
  steem.api.send(mapping[0].api, {
    method: method,
    params: params,
  }, (err, result) => {
    res.send({
      jsonrpc: '2.0',
      id,
      method,
      result,
    });
  });
});

router.post('/broadcast/follow', (req, res) => {
   const query = parseQuery(req.query);
   const mappedParams = [
      req.body['wif'],
      [],
      [req.body['follower']],
      "follow",
      JSON.stringify(
          ['follow', {
            follower: req.body['follower'],
            following: req.body['following'],
            what: req.body['what']
          }]
        )
   ];
  // Add response handler
  mappedParams.push((err, result) => {
    if(err != null) {
       console.log(req.body);
       res.status(500).send(err.message);
    } else {
       const json = query.scope
         ? result[query.scope] : result;
       res.json(json);
    }
  });
   steem.broadcast.customJson.apply(steem.broadcast, mappedParams);
});

router.post('/broadcast/:operation', (req, res) => {
  const query = parseQuery(req.query);
  const operation = decamelize(req.params.operation, '_');
  const mapping = _.filter(broadcastOperations, { operation: operation });
  var mappedParams = [];
  try {
     const mappingParams = mapping[0].params.slice();
     mappingParams.unshift('wif');
     _.forEach(mappingParams, function(p) {
        if(typeof req.body[p] == 'undefined') { throw 'Parameter '+p+' is undefined. Required params: ['+mapping[0].params.join(',')+']'; }
        if(req.body[p] == null) { mappedParams.push(undefined); } else { mappedParams.push(req.body[p]); }
     });
  } catch(err) {
     res.status(400).send({
       'error': err
     });
     return;
  }
  // Add response handler
  mappedParams.push((err, result) => {
    if(err != null) {
       console.log(req.body);
       res.status(500).send(err.message);
    } else {
       const json = query.scope
         ? result[query.scope] : result;
       res.json(json);
    }
  });
  steem.broadcast[camelize(operation)].apply(steem.broadcast, mappedParams);
});

router.post('/auth/:auth_method', (req, res) => {
  const query = parseQuery(req.query);
  const auth_method = decamelize(req.params.auth_method, '_');
  const mapping = _.filter(authMethods, { auth_method: auth_method });
  var mappedParams = [];
  try {
     const mappingParams = mapping[0].params.slice();
     _.forEach(mappingParams, function(p) {
        if(typeof req.body[p] == 'undefined') { throw 'Parameter '+p+' is undefined. Required params: ['+mapping[0].params.join(',')+']'; }
        mappedParams.push(req.body[p]);
     });
  } catch(err) {
     res.status(400).send({
       'error': err
     });
     return;
  }
  res.send({"result": steem.auth[camelize(auth_method)].apply(steem.auth, mappedParams)});
});

router.get('/:method', (req, res) => {
  const query = parseQuery(req.query);
  const method = decamelize(req.params.method, '_');
  const mapping = _.filter(methods, { method: method });
  let params = [];
  if (mapping[0].params) {
    mapping[0].params.forEach((param) => {
      const queryParam = req[param] || query[param] || query[decamelize(param)];
      params.push(queryParam);
    });
  }
  let main = function(params) {
     steem.api.send(mapping[0].api, {
       method: method,
       params: params
     }, (err, result) => {
       try {
          const json = query.scope
            ? result[query.scope] : result;
          if(query.image_only == true) {
             let imageJson = _.map(json, function(x, y){
                let result = { author: x.author, permlink: x.permlink };
                let metadata = JSON.parse(x.json_metadata);
                let active_votes = _.map(x.active_votes, function(v) { return { 'voter': v.voter, 'time': v.time, 'weight': v.weight }; });
                if(typeof metadata.image == 'undefined') { metadata.image = 'https://placeholdit.co/i/250x150' }
                result.image = typeof metadata.image == 'string' ? metadata.image : metadata.image[0];
                if(query.include_body == true) {
                    result.created = x.created;
                    result.net_votes = x.net_votes;
                    result.tags = metadata.tags;
                    result.title = x.title;
                    result.body = x.body;
                    result.pending_payout_value = x.pending_payout_value;
                    result.total_payout_value = x.total_payout_value;
                    result.cashout_time = x.cashout_time;
                    result.active_votes = active_votes;
                }
                return result;
             });
             joinAuthors(query, imageJson, function(newJson) {
                res.json(newJson);
             });
          } else {
             if(typeof json != 'undefined' && typeof json.json_metadata != 'undefined' && json.json_metadata != '') {
                let modifiedJson = json;
                let metadata = JSON.parse(json.json_metadata);
                if(typeof metadata.image == 'undefined') { metadata.image = 'https://placeholdit.co/i/250x150' }
                metadata.image = typeof metadata.image == 'string' ? metadata.image : metadata.image[0];
                modifiedJson.json_metadata = metadata;
                joinAuthors(query, modifiedJson, function(newJson) {
                  res.json(newJson);
                });
             } else if(typeof json[0] != 'undefined' && typeof json[0].json_metadata != 'undefined' && json[0].json_metadata != '') {
                let modifiedJson = json;
                let metadata = JSON.parse(json[0].json_metadata);
                modifiedJson[0].json_metadata = metadata;
                modifiedJson = addPlaceholderImage(method, modifiedJson);
                joinAuthors(query, modifiedJson, function(newJson) {
                  res.json(newJson);
                });
             } else {
                modifiedJson = addPlaceholderImage(method, json);
                joinAuthors(query, modifiedJson, function(newJson) {
                  res.json(newJson);
                });
             }
          }
       } catch(err) {
          res.status(500).send('Error: '+err);
       }
     });
  };
  if(typeof query['following'] != 'undefined') {
    steem.api.getFollowing(query['following'], '', 'blog', 99, function(err, result) {
      if(!err) {
         if(result.length > 0) {
            params[0].select_authors = _.map(result, function(v) {
               return v.following;
            });
         } else {
            params[0].select_authors = [ 'null' ]; // User has no followers so we want to return nothing
         }
      } else {
         params[0].select_authors = [ 'null' ];
         console.log(err, result);
      }
      main(params);
    });
  } else {
    main(params);
  }
});

const addPlaceholderImage = (method, json) => {
    if(method != 'get_accounts') { return json; }
    var modifiedJson = json;
    if(typeof json[0] != 'undefined') {
        modifiedJson = _.map(json, function(v) {
            if(v.json_metadata == '') {
                v.json_metadata = {
                    'profile': {
                        'profile_image': 'https://placeholdit.co/i/250x150'
                    }
                };
            } else {
                if(typeof v.json_metadata.profile == 'undefined') { v.json_metadata.profile = {}; }
                if(typeof v.json_metadata.profile.profile_image == 'undefined') { v.json_metadata.profile.profile_image = 'https://placeholdit.co/i/250x150'; }
            }
            return v;
        });
    }
    return modifiedJson;
}

const joinAuthors = (query, json, callback) => {
   if(query.include_author_profile_image == true) {
      var names = [];
      if(typeof json.author != 'undefined' && json.author != '') {
         names = [json.author];
      } else if(typeof json[0] != 'undefined' && typeof json[0].author != 'undefined' && json[0].author != '') {
         names = _.map(json, function(v) {
            return v.author;
         });
      } else if(typeof json[0] != 'undefined' && typeof json[0].follower != 'undefined' && json[0].follower != '') {
         names = _.map(json, function(v) {
            return v.follower;
         });
      }
      names = _.uniqBy(names, function (e) { return e; });
      steem.api.getAccounts(names, function(err, result) {
         if(err) { return callback(json); }
         var joinAuthor = function(post) {
            for(var a in result) {
               if(result[a].name == post.author || result[a].name == post.follower) {
                  let metadata = {};
                  if(typeof result[a].json_metadata == 'string' && result[a].json_metadata != '') {
                     metadata = JSON.parse(result[a].json_metadata);
                  } else {
                     metadata = result[a].json_metadata;
                  }
                  if(typeof metadata.profile != 'undefined' && typeof metadata.profile.profile_image != 'undefined') {
                     post.author_profile_image = metadata.profile.profile_image;
                     post.author_display_name = metadata.profile.name
                  } else {
                     post.author_profile_image = 'https://placeholdit.co/i/250x150';
                     post.author_display_name = post.author;
                  }
               }
            }
            return post;
         };
         if(typeof json.author != 'undefined' && json.author != '') {
            // Single object
            callback(joinAuthor(json));
         } else if((typeof json[0] != 'undefined' && typeof json[0].author != 'undefined' && json[0].author != '') || (typeof json[0] != 'undefined' && typeof json[0].follower != 'undefined' && json[0].follower != '')) {
            // Array of objects
            var modifiedJson = json;
            _.map(modifiedJson, function(v) {
               return joinAuthor(v);
            });
            callback(modifiedJson);
         } else {
            callback(json);
         }
      });
   } else {
      callback(json);
   }
};

const parseQuery = (query) => {
  let newQuery = {};
  Object.keys(query).map(key => {
    let value = query[key];
    try { value = JSON.parse(decodeURIComponent(value)); }
    catch (e) { }
    newQuery[key] = value;
  });
  return newQuery;
};

module.exports = router;
